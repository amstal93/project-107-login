<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211229131935 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE departments (dept_no INT NOT NULL, dept_name VARCHAR(40) NOT NULL, PRIMARY KEY(dept_no)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dept_emp (id INT AUTO_INCREMENT NOT NULL, emp_no INT NOT NULL, dept_no INT NOT NULL, from_date DATE DEFAULT NULL, to_date DATE DEFAULT NULL, INDEX IDX_B2592B4DA2F57F47 (emp_no), INDEX IDX_B2592B4DE6B0AD08 (dept_no), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dept_manager (id INT AUTO_INCREMENT NOT NULL, emp_no INT NOT NULL, dept_no INT NOT NULL, from_date DATE DEFAULT NULL, to_date DATE DEFAULT NULL, INDEX IDX_C14AA78EA2F57F47 (emp_no), INDEX IDX_C14AA78EE6B0AD08 (dept_no), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employees (emp_no INT NOT NULL, birth_date DATE DEFAULT NULL, first_name VARCHAR(14) DEFAULT NULL, last_name VARCHAR(16) DEFAULT NULL, gender VARCHAR(1) DEFAULT NULL, hire_date DATE DEFAULT NULL, PRIMARY KEY(emp_no)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE salaries (id INT AUTO_INCREMENT NOT NULL, emp_no INT NOT NULL, salary INT NOT NULL, from_date DATE DEFAULT NULL, to_date DATE DEFAULT NULL, INDEX IDX_E6EEB84BA2F57F47 (emp_no), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE titles (id INT AUTO_INCREMENT NOT NULL, emp_no INT NOT NULL, title VARCHAR(40) NOT NULL, from_date DATE DEFAULT NULL, to_date DATE DEFAULT NULL, UNIQUE INDEX UNIQ_C14541A32B36786B (title), INDEX IDX_C14541A3A2F57F47 (emp_no), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(50) NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(254) NOT NULL, full_name VARCHAR(50) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dept_emp ADD CONSTRAINT FK_B2592B4DA2F57F47 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dept_emp ADD CONSTRAINT FK_B2592B4DE6B0AD08 FOREIGN KEY (dept_no) REFERENCES departments (dept_no) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dept_manager ADD CONSTRAINT FK_C14AA78EA2F57F47 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dept_manager ADD CONSTRAINT FK_C14AA78EE6B0AD08 FOREIGN KEY (dept_no) REFERENCES departments (dept_no) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE salaries ADD CONSTRAINT FK_E6EEB84BA2F57F47 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE titles ADD CONSTRAINT FK_C14541A3A2F57F47 FOREIGN KEY (emp_no) REFERENCES employees (emp_no) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE dept_emp DROP FOREIGN KEY FK_B2592B4DE6B0AD08');
        $this->addSql('ALTER TABLE dept_manager DROP FOREIGN KEY FK_C14AA78EE6B0AD08');
        $this->addSql('ALTER TABLE dept_emp DROP FOREIGN KEY FK_B2592B4DA2F57F47');
        $this->addSql('ALTER TABLE dept_manager DROP FOREIGN KEY FK_C14AA78EA2F57F47');
        $this->addSql('ALTER TABLE salaries DROP FOREIGN KEY FK_E6EEB84BA2F57F47');
        $this->addSql('ALTER TABLE titles DROP FOREIGN KEY FK_C14541A3A2F57F47');
        $this->addSql('DROP TABLE departments');
        $this->addSql('DROP TABLE dept_emp');
        $this->addSql('DROP TABLE dept_manager');
        $this->addSql('DROP TABLE employees');
        $this->addSql('DROP TABLE salaries');
        $this->addSql('DROP TABLE titles');
        $this->addSql('DROP TABLE user');
    }
}
