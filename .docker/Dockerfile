FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
ARG PHP_VERSION=8.0
ARG UID=1000
ARG GID=1000

###
# INSTALL PACKAGES
###
RUN set -x && apt-get update && apt-get install -y gnupg

RUN echo "deb http://ppa.launchpad.net/ondrej/php/ubuntu focal main" > /etc/apt/sources.list.d/ppa_ondrej_php.list \
    && echo "deb http://nginx.org/packages/ubuntu focal nginx" > /etc/apt/sources.list.d/ppa_nginx_mainline.list \
    && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys E5267A6C \
    && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys ABF5BD827BD9BF62

RUN  apt-get update \
    && apt-get install -y curl wget zip unzip git supervisor sqlite3 nano cron iputils-ping telnet python build-essential

###
# PHP
###
RUN  apt-get install -y nginx php${PHP_VERSION}-fpm php${PHP_VERSION}-cli \
    php${PHP_VERSION}-pgsql php${PHP_VERSION}-sqlite3 php${PHP_VERSION}-gd \
    php${PHP_VERSION}-curl php${PHP_VERSION}-memcached php${PHP_VERSION}-soap \
    php${PHP_VERSION}-imap php${PHP_VERSION}-mysql php${PHP_VERSION}-mbstring \
    php${PHP_VERSION}-xml php${PHP_VERSION}-zip php${PHP_VERSION}-bcmath \
    php${PHP_VERSION}-intl php${PHP_VERSION}-readline php${PHP_VERSION}-xdebug \
    php${PHP_VERSION}-msgpack php${PHP_VERSION}-igbinary php${PHP_VERSION}-ldap \
    php${PHP_VERSION}-redis php${PHP_VERSION}-ftp php${PHP_VERSION}-imagick \
    php${PHP_VERSION}-mongodb

RUN mkdir -p /run/php

###
# NGINX
###
RUN apt-get install -y nginx && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

###
# COMPOSER
###
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('sha384', 'composer-setup.php') === '$(curl -sL https://composer.github.io/installer.sig)') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php -- --install-dir=/usr/bin/ --filename=composer \
    && php -r "unlink('composer-setup.php');"

###
# NODE JS
###
RUN curl -fsSL https://deb.nodesource.com/setup_15.x | bash - \
    && apt install -y nodejs

###
# GOSU
###
RUN apt-get install gosu && \
    chown root:www-data /usr/sbin/gosu && \
    chmod +s /usr/sbin/gosu

###
# CELEANUP
###
RUN apt-get remove -y --purge software-properties-common \
    && apt-get -y autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p /var/www/html \
    && chown www-data:www-data /var/www -R

RUN usermod -u ${UID} www-data && groupmod -g ${GID} www-data && usermod -a -G ${GID} www-data

WORKDIR /var/www/html

###
# COPY CONFIG FILES AND SCRIPTS
###
COPY nginx/default.conf.dev /etc/nginx/sites-available/default.conf
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY php/xdebug.ini /etc/php/${PHP_VERSION}/mods-available/xdebug.ini
COPY php/php.ini /etc/php/${PHP_VERSION}/cli/conf.d/90-custom-php.ini
COPY php/php.ini /etc/php/${PHP_VERSION}/fpm/conf.d/90-custom-php.ini
COPY supervisord/supervisord.conf.prod /etc/supervisor/conf.d/supervisord.conf.prod
COPY supervisord/supervisord.conf.dev /etc/supervisor/conf.d/supervisord.conf.dev
COPY docker-start-script /usr/bin/docker-start-script
COPY docker-entrypoint /usr/bin/docker-entrypoint

RUN chmod +x /usr/bin/docker-start-script
RUN chmod +x /usr/bin/docker-entrypoint

###
# LAUNCH
###
EXPOSE 80

CMD ["docker-start-script"]
ENTRYPOINT ["docker-entrypoint"]
