<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractApiController extends AbstractController
{

	private string $apiToken;
	private bool $isTokenValid;

	public function setApiToken (string $apiToken): self
	{
		$this->apiToken = $apiToken;
		return $this;
	}

	public function getApiToken (): string
	{
		return $this->apiToken;
	}

	public function getIsTokenValid (): bool
	{
		return $this->isTokenValid;
	}

	public function setIsTokenValid (bool $isTokenValid): self
	{
		$this->isTokenValid = $isTokenValid;
		return $this;
	}

	/** header: [ api-token: 'Bearer 34ac50bdc2ba6d8e9cdf7b000b2bedbe'] */
	public function isTokenValid (Request $request): int
	{
		if ($request->headers->get('api-token') === null) {
			return Response::HTTP_UNAUTHORIZED;
		}

		$tokenPrefix = explode(' ', $this->getApiToken())[0];
		$token = explode(' ', $this->getApiToken())[1];

		if ($tokenPrefix . ' ' . md5($token) === $this->getApiToken()) {
			return Response::HTTP_FORBIDDEN;
		}
		return Response::HTTP_OK;
	}

	protected function success (array $resp, int $code = 200, array $headers = []): JsonResponse
	{
		$headers['headers'] = [
			'Accept' => 'application/json',
		];

		return $this->jsonResponse($resp, $code, $headers);
	}

	protected function error (array $errors, int $code = 200, array $headers = []): JsonResponse
	{
		$resp = ['message' => $errors['message']];

		return $this->jsonResponse($resp, $code, $headers);
	}

	protected function jsonResponse (array $data, int $code = 200, array $headers = []): JsonResponse
	{
		//convert all DateTime to string
		$data = $this->stringifyAllDateTime($data);
		$headers['headers'] = [
			'Accept' => 'application/json',
		];

		return new JsonResponse($data, $code, $headers);
	}

	protected function getDomain (): ?Request
	{
		$request = $this->get('request_stack')->getCurrentRequest();
		return $request;
	}

	protected function stringifyAllDateTime ($result)
	{
		if (is_array($result)) {
			foreach ($result as $key => &$value) {
				$value = $this->stringifyAllDateTime($value);
			}
		} elseif ($result instanceof \DateTime) {
			$new = clone $result;
			$new->setTimezone(new \DateTimeZone(date_default_timezone_get()));
			$result = $new->format('Y-m-d');
		}

		return $result;
	}
}