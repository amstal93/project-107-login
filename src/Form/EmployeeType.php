<?php

namespace App\Form;

use App\Entity\Employee;
use Symfony\Component\Form\Extension\Core\Type as FormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployeeType extends AbstractType
{
	use BaseTypeTrait;

	public function buildForm (FormBuilderInterface $builder, array $options): void
	{

		if ($options['data']->getEmpNo()) {
			$builder
				->add('empNo', FormType\IntegerType::class,
					[
						'label' => 'Employee no',
						'required' => true,
						'disabled' => true,
					]);
		} else {
			$builder
				->add('empNo', FormType\IntegerType::class,
					[
						'label' => 'Employee no',
						'required' => true,
					]);
		}


		$builder
			->add('birthDate', FormType\DateType::class,
				[
		            'label' => 'Birth date',
					'required' => true,
					'years' => $this->getPastYearsAndCurrent(50),
					'format' => 'y-M-d',
				])
			->add('firstName')
			->add('lastName')
			->add('gender', FormType\ChoiceType::class, [
				'choices' => [
					' ' => null,
					'Man' => 'm',
					'Woman' => 'w',
				],
				'required' => true,
			])
			->add('hireDate', FormType\DateType::class,
				[
					'required' => true,
					'years' => $this->getPastYearsAndCurrent(),
					'format' => 'y-M-d',
				])
			->add('save', FormType\SubmitType::class, [
				'label' => 'Save',
				'attr' => array('class' => 'btn btn-primary btn-sm')
			]);
	}

	public function configureOptions (OptionsResolver $resolver): void
	{
		$resolver->setDefaults([
			'data_class' => Employee::class,
		]);
	}
}
