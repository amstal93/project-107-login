<?php

namespace App\Form;

use App\Entity\Department;
use App\Entity\DeptEmp;
use App\Entity\Employee;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as FormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class DeptEmpType extends AbstractType
{
	use BaseTypeTrait;
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
	        ->add('empNo', EntityType::class, [
		        'class' => Employee::class,
		        'choice_label' => function ($employee) {
			        return $employee->getFullName();
		        },
		        'choice_value' => 'empNo',
		        'label' => 'Employee',
		        'required' => true,
	        ])
	        ->add('deptNo', EntityType::class, [
		        'class' => Department::class,
		        'choice_label' => function ($department) {
			        return $department->getDeptName();
		        },
		        'choice_value' => 'deptNo',
		        'label' => 'Department',
		        'required' => true,
	        ])
	        ->add('fromDate',FormType\DateType::class,
		        [
			        'label' => 'Form date',
			        'required' => true,
			        'years' => $this->getPastYearsAndCurrent(),
			        'format' => 'y-M-d',
		        ])
	        ->add('toDate',FormType\DateType::class,
		        [
			        'label' => 'Form date',
			        'required' => true,
			        'years' => $this->getPastYearsAndCurrent(),
			        'format' => 'y-M-d',
		        ])
	        ->add('save',FormType\SubmitType::class, [
		        'label' => 'Save',
		        'attr' => array('class' => 'btn btn-primary btn-sm')
	        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DeptEmp::class,
        ]);
    }
}
