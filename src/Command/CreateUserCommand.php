<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserCommand extends Command
{
	// the name of the command (the part after "bin/console")
	protected static $defaultName = 'app:create-user';

	/** @var EntityManager */
	private $em;

	/** @var UserPasswordEncoderInterface */
	private $passwordEncoder;

	public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
	{
		parent::__construct();

		$this->em = $em;
		$this->passwordEncoder = $passwordEncoder;
	}

	protected function configure()
	{
		$this
			->setDescription('Set required and optional user attributes and create it. '."\n\t".'For example: php bin/console app:create-user ize jelszo 0 ize@ize.hu "ize Bigyó"')
			->addArgument('user_name', InputArgument::REQUIRED, 'required user field')
			->addArgument('user_password', InputArgument::REQUIRED, 'required user password')
			->addArgument('user_role', InputArgument::REQUIRED, 'required user role [0:user, 1:admin]')
			->addArgument('user_email', InputArgument::REQUIRED, 'required user email')
			->addArgument('user_full_name', InputArgument::REQUIRED, 'required user full name')
			->setHelp('This command allows create a user...');
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$io = new SymfonyStyle($input, $output);
		$output->writeln([
			'============',
			'User Creator',
			'============',
			'',
		]);

		$this->createUser($input);

		$io->success(sprintf('Created User! Your new username: %s and password %s', $input->getArgument('user_name'), $input->getArgument('user_password')));
		return 0;
    }

	protected function createUser(InputInterface $input)
	{
		try {
			/** @var User $user */
			$user = new User;
			$user->setUsername($input->getArgument('user_name'));
			$user->setPassword($this->passwordEncoder->encodePassword($user, $input->getArgument('user_password')));
			$user->setRoles([$input->getArgument('user_role') ? User::ROLE_ADMIN : User::ROLE_USER]);
			$user->setEmail($input->getArgument('user_email'));
			$user->setFullName($input->getArgument('user_full_name'));

			$this->em->persist($user);
			$this->em->flush();

		} catch (\Exception $ex) {
			(new ConsoleOutput())->writeln("<error>Error: {$ex->getMessage()}</error>");
			die();
		}
	}
}
