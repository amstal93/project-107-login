<?php

namespace App\Entity;

use App\Repository\DeptEmpRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=DeptEmpRepository::class)
 * @ORM\Table(name="dept_emp")
 */
class DeptEmp
{
	use DateIntervalTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;


	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Employee", inversedBy="empNo")
	 * @ORM\JoinColumn(nullable=false, name="emp_no", referencedColumnName="emp_no", onDelete="CASCADE")
	 * @Assert\NotBlank()
	 */
	private $empNo;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Department", inversedBy="deptNo")
	 * @ORM\JoinColumn(nullable=false, name="dept_no", referencedColumnName="dept_no", onDelete="CASCADE")
	 * @Assert\NotBlank()
	 */
	private $deptNo;

    public function getId(): ?int
    {
        return $this->id;
    }


	public function getEmpNo(): ?employee
	{
		return $this->empNo;
	}

	public function setEmpNo(?employee $empNo): self
	{
		$this->empNo = $empNo;

		return $this;
	}


	public function getDeptNo(): ?department
	{
		return $this->deptNo;
	}

	public function setDeptNo(?department $deptNo): self
	{
		$this->deptNo = $deptNo;

		return $this;
	}
}
