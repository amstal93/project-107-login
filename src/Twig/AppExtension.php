<?php


namespace App\Twig;


use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension implements GlobalsInterface
{

	public function getFilters(): array
	{
		return [
			new TwigFilter('price', [$this, 'priceFilter']),
			new TwigFilter('gender', [$this, 'genderFilter']),
			new TwigFilter('age', [$this, 'ageFilter'])
		];
	}

	public function getGlobals(): array
	{
		return[];
	}

	public function priceFilter($number): string
	{
		return '$'.number_format($number, 2, '.', ',');
	}

	public function genderFilter(string $gender): string
	{
		switch ($gender) {
			case 'w':
				return 'woman';
			case 'm':
				return 'man';
			default:
				return '';
		}
	}

	public function ageFilter(string $age)
	{
		$date1 = (new \DateTime())->format('Y-m-d');
		$date2 = (new \DateTime($age))->format('Y-m-d');

		$diff = abs(strtotime($date2) - strtotime($date1));

		return floor($diff / (365*60*60*24));
	}
}